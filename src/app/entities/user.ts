export class User {
    public name: string;
    public last_name: string;
    public country: string;
    public province: string;
    public mail: string;
    public phone: string;
    public password: string;

    constructor() {
        this.name = null;
        this.last_name = null;
        this.country = null;
        this.province = null;
        this.mail = null;
        this.phone = null;
        this.password = null;
    }
}
