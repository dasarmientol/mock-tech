import { Authorization } from './../interfaces/authorization';
export class Session {

    /*
    Email del usuario a loggearse
    */
    public email: string;

    /*
    Password del usuario a loggearse
    */
    public password: string;

    /*
    Autorizacion para navegar en la app
    */
    public token: Authorization;

    constructor() {
        this.email = null;
        this.password = null;
        this.token = null;
    }
}
