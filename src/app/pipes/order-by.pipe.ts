import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {

  async transform(value: any[], descending?: boolean, attr?: string): Promise<any> {
    return await this.filterList(value, descending, attr);
  }

  /**
   * Metodo que se encarga de filtrar el listado en orden asc o desc
   *
   * @author Diego Sarmiento - Jul. 08-2021
   *
   * @param value Valor a filtrar
   * @param descending Indica el orden
   * @param attr Es el atributo a filtrar
   */
  filterList(value: any[], descending?: boolean, attr?: string): Promise<any>{
    return new Promise((resolve) => {
      value.sort((t1, t2) => {
        const name1 = t1[attr];
        const name2 = t2[attr];
        if (descending) {
          if (name1 > name2) { return 1; }
          if (name1 < name2) { return -1; }
        } else {
          if (name1 > name2) { return -1; }
          if (name1 < name2) { return 1; }
        }
        return 0;
      });

      resolve(value);
    });
  }
}
