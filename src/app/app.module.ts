import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { PanelLayoutComponent } from './components/panel-layout/panel-layout.component';
import { DashboardComponent } from './pages/home/dashboard/dashboard.component';
import { ListTechComponent } from './pages/home/list-tech/list-tech.component';
import { TechViewComponent } from './pages/home/tech-view/tech-view.component';
import { LoginComponent } from './pages/public/login/login.component';
import { SignUpComponent } from './pages/public/sign-up/sign-up.component';
import { HttpInterceptorService } from './services/http-interceptor.service';
import { NormalizeTextPipe } from './pipes/normalize-text.pipe';
import { SearchPipe } from './pipes/search.pipe';
import { OrderByPipe } from './pipes/order-by.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    ListTechComponent,
    TechViewComponent,
    HeaderComponent,
    FooterComponent,
    PanelLayoutComponent,
    SignUpComponent,
    NormalizeTextPipe,
    SearchPipe,
    OrderByPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
