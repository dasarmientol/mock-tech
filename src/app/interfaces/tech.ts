export interface Tech {
    /*
    Nombre de la tecnologia
    */
    tech: string;

    /*
    Anio de creacion de la tecnologia
    */
    year: string;

    /*
    Creador de la tecnologia
    */
    author: string;

    /*
    Licencia de la tecnologia
    */
    license: string;

    /*
    Lenguaje que soporta la tecnologia
    */
    language: string;

    /*
    Tipo de tecnologia
    */
    type: string;

    /*
    Logo de la tecnologia
    */
    logo: string;
}
