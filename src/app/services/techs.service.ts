import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { Tech } from '../interfaces/tech';

@Injectable({
  providedIn: 'root'
})
export class TechsService {

  constructor(
    private http: HttpClient
  ) { }

  public getTechs(): Promise<Array<Tech>> {
    return this.http.get<Array<Tech>>(environment.api.base + '/techs').toPromise();

  }
}
