import { Injectable } from '@angular/core';

import { User } from '../entities/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor() { }

  /**
   * Guarda al usuario registrado en el localStorage
   *
   * @param user Usuario registrado
   *
   * @author Diego Sarmiento - Jul. 08-2021
   */
  public saveUser(user: User): void {
      localStorage.setItem('user', JSON.stringify(user));
  }

  /**
   * Metodo que carga al usuario cargado en el localStorage
   *
   * @author Diego Sarmiento - Jul. 08-2021
   */
  public loadUser(): User {
    const userLocal = localStorage.getItem('user');
    return JSON.parse(userLocal);
  }
}
