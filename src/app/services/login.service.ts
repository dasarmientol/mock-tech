import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { Authorization } from '../interfaces/authorization';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private http: HttpClient
  ) { }

  public sendAuthentication(data): Promise<Authorization> {
    return this.http.post<Authorization>(environment.api.base + '/signup', data).toPromise();
  }

  public sendLogin(data): Promise<Authorization> {
    return this.http.post<Authorization>(environment.api.base + '/login', data).toPromise();
  }

}
