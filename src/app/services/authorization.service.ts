import { Injectable } from '@angular/core';

import { Session } from '../entities/session';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  private session: Session;

  constructor() {
    this.session = new Session();
    this.loadSession();
  }

  /**
   * Metodo que guarda la session en el localStorage
   *
   * @author Diego Sarmiento - Jul. 08-2021
   */
  public saveSession(): void {
    if (this.session !== null) {
      localStorage.setItem('session', JSON.stringify(this.session));
    }
  }

  /**
   * Metodo que carga la session del localStorage
   *
   * @author Diego Sarmiento - Jul. 08-2021
   */
  public loadSession(): void {
    const session = localStorage.getItem('session');
    if (session !== null) {
      this.session = JSON.parse(session);
    }
  }

  /**
   * Obtiene la session
   *
   * @author Diego Sarmiento - Jul. 08-2021
   */
  public get autorization(): Session {
    return this.session;
  }

  /**
   * Modifica la session
   *
   * @param session Session del usuario
   *
   * @author Diego Sarmiento - Jul. 08-2021
   */
  public set autorization(session: Session) {
    this.session = session;
  }
}
