import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PanelLayoutComponent } from './components/panel-layout/panel-layout.component';
import { DashboardComponent } from './pages/home/dashboard/dashboard.component';
import { ListTechComponent } from './pages/home/list-tech/list-tech.component';
import { LoginComponent } from './pages/public/login/login.component';
import { SignUpComponent } from './pages/public/sign-up/sign-up.component';

const accountPanelRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'techs', component: ListTechComponent },

];

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'home', component: PanelLayoutComponent, children: accountPanelRoutes },
  { path: 'sign-up', component: SignUpComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
