/// <reference lib="webworker" />

export class SearchWorker {

  constructor() { }

  /**
   * Se empieza a realizar el filtro de busqueda con un filtro recursivo
   *
   * @param search Filtro de busqueda
   * @param items Arreglo con datos en los cuales se va a realizar la busqueda
   */
  searchInWorker(search: string, items) {

    // si no estan mandando nada a buscar entonces devolvemos el mismo valor ingresado
    if (search == null) {
      return items;
    }

    // creamos una validacion que contenga algo la busqueda aparte para que no choque en la validacion anterior en caso de que sea null
    if (search.trim() === '') {
      return items;
    }

    // filtramos para devolver
    return items.filter(item => {
      return this.filter(search, item);
    });
  }

  /**
   * Permite buscar de manera recursiva
   *
   * @param search Filtro de busqueda
   * @param item Dato en el cual se va a realizar la busqueda
   */
  private filter(search: string, item): boolean {

    // validamos si lo que se esta trabajando es un arreglo de objetos primitivos
    if (this.isPrimitive(item)) {
      return String(item).toLocaleLowerCase().includes(String(search).toLocaleLowerCase());
    }

    // obtenemos las llaves de cada item
    const itemsKeys = Object.keys(item);

    // iteramos las llaves
    for (const keyName of itemsKeys) {

      // si el valor de la propiedad es primitivo
      if (this.isPrimitive(item[keyName])) {

        // verificamos si tienen alli lo que estamos buscando
        if (String(item[keyName]).toLocaleLowerCase().includes(String(search).toLocaleLowerCase())) {
          return true;
        }

      } else {

        // como no es primitivo, entonces ejecutamos otra busqueda internamente alli
        if (this.filter(search, item[keyName])) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Permite verificar si una variable es primitiva
   *
   * @param customVar Variable del objeto donde se esta realizando el filtro
   */
  private isPrimitive(customVar): boolean {

    if (customVar === null) {
      return true;
    }

    switch (typeof customVar) {
      case 'boolean':
      case 'number':
      case 'string':
        return true;
        break;
      default:
        return false;
        break;
    }
  }
}

/**
 * Evento donde llega el mensaje cuando ha sido invocado el worker
 */
addEventListener('message', ({ data }) => {

  // Filtro que se va a realizar
  const search = data.search !== null ? (data.search).toLowerCase() : '';

  // Arreglo de objetos donde se va a realizar la busqueda
  const items = data.items;

  // Se crea un objeto para realizar el filtro de busqueda
  const searchWorker = new SearchWorker();
  // Se invoca el metodo del worker para realizar la busqueda
  const response = searchWorker.searchInWorker(search, items);

  // Se envia el resultado
  postMessage(response);
});
