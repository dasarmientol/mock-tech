import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Tech } from '../../../interfaces/tech';
import { TechsService } from '../../../services/techs.service';

@Component({
  selector: 'app-list-tech',
  templateUrl: './list-tech.component.html',
  styleUrls: ['./list-tech.component.scss']
})
export class ListTechComponent implements OnInit {

  /*
  Arreglo donde van a obtenersen las tecnologias
  */
  public techs: Tech[];

  /*
  Permite realizar la busqueda de tecnologias
  */
  public searchTech: string;

  public orderAlphabetical: boolean;

  /**
   * Constructor de la clase
   *
   * @param techsService Servicio para obtener las tecnologias
   */
  constructor(
    private router: Router,
    private techsService: TechsService
  ) {
    this.techs = [];
    this.searchTech = '';
    this.orderAlphabetical = true;
  }

  ngOnInit(): void {
    // Se llama el metodo encargado de obtener las tecnologias para ser mostradas en vista
    this.loadTechs();
  }

  /**
   * Metodo que carga las tecnologias
   *
   * @author Diego Sarmiento - Jul. 07-2021
   */
  public loadTechs(): void {

    // Se consulta el servicio para obtener las tecnologias
    this.techsService.getTechs()
      .then((response) => {
        // Obtiene las tecnologias y se guardan en el arreglo para mostrarse en la vista
        this.techs = response;
      })
      .catch((error) => {
        console.error('error', error);
      })
      .finally(() => {
      });
  }

  public openDetailTech(idTech: string): void {
    this.router.navigateByUrl('tech-view/' + idTech);
  }

  /**
   * Realiza el cambio del filtro de ordenamiento
   *
   * @author Diego Sarmiento - Jul. 08-2021
   */
  public changeOrderAlphabeticalDesc(): void {
    setTimeout(() => {
      this.orderAlphabetical = true;
    }, 1000);
  }

  /**
   * Realiza el cambio del filtro de ordenamiento
   *
   * @author Diego Sarmiento - Jul. 08-2021
   */
  public changeOrderAlphabeticalAsc(): void {
    setTimeout(() => {
      this.orderAlphabetical = false;
    }, 1000);
  }

}
