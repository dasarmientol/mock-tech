import { City, Country, State } from 'country-state-city';
import { ICountry, IState } from 'country-state-city/dist/lib/interface';

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { User } from '../../../entities/user';
import { LoginService } from '../../../services/login.service';
import { UsersService } from 'src/app/services/users.service';

declare const Snackbar;
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  /*
  Formulario que contiene las validaciones de los campos ingresados por el usuario
  */
  public registerForm: any;

  /*
  Arreglo de paises
  */
  public countries: ICountry[];

  /*
  Pais seleccionado
  */
  public countrySelect: ICountry;

  /*
  Arreglo de estados
  */
  public states: IState[];

  /*
  Estado seleccionado
  */
  public stateSelect: IState;
  /**
   * Constructor de la clase
   *
   * @author Diego Sarmiento - Jun. 30-2021
   *
   * @param formBuilder Proporciona metodos para crear controles en un formulario
   */
  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private usersService: UsersService
  ) {

    // Se crea la estructura del formulario
    this.registerForm = this.formBuilder.group({
      name: ['', {
        validators: [
          Validators.required,
          Validators.pattern('[a-zA-Z ]{2,30}'),
          Validators.maxLength(30)
        ]
      }],
      last_name: ['', {
        validators: [
          Validators.required,
          Validators.pattern('[a-zA-Z ]{2,30}'),
          Validators.maxLength(30)
        ]
      }],
      country: ['', {
        validators: [
          Validators.required,
        ]
      }],
      province: ['', {
        validators: [
          Validators.required,
        ]
      }],
      mail: ['', {
        validators: [
          Validators.required,
          Validators.email
        ]
      }],
      phone: ['', {
        validators: [
          Validators.required,
          Validators.pattern('^[0-9]*$'),
          Validators.minLength(8),
          Validators.maxLength(10)
        ]
      },
      ],
      password: ['']
    });
  }

  ngOnInit(): void {
    this.countries = Country.getAllCountries();
  }

  public signUp(): void {

    if (this.registerForm.status === 'INVALID') {
      const x = document.getElementById('snackbar-failed');

      x.className = 'show';

      setTimeout(() => {
        x.className = x.className.replace('show', '');
      }, 3000);
    } else {

      const data: User = {
        name: this.registerForm.name.value,
        last_name: this.registerForm.last_name.value,
        country: this.registerForm.country.value,
        province: this.registerForm.province.value,
        mail: this.registerForm.mail.value,
        phone: this.registerForm.phone.value,
        password: this.registerForm.password.value,
      };

      this.loginService.sendAuthentication(data)
        .then((response) => {
          this.usersService.saveUser(data);

        })
        .catch((error) => {
          console.error('error', error);
        })
        .finally(() => {
        });
    }

  }

  public selectCountry(): void {
    this.states = State.getStatesOfCountry(this.countrySelect.isoCode);
  }
}
