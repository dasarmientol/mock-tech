import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Session } from '../../../entities/session';
import { AuthorizationService } from '../../../services/authorization.service';
import { LoginService } from '../../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  /*
  Usuario a loggearse
  */
  public session: Session;

  constructor(
    public authorizationService: AuthorizationService,
    private loginService: LoginService,
    private router: Router
  ) {
    this.session = new Session();
  }

  ngOnInit(): void {
  }

  /**
   * Metodo para realizar el login de la app
   *
   * @author Diego Sarmiento - Jul. 08-2021
   */
  public login(): void {
    this.loginService.sendLogin(this.session)
      .then((response) => {
        if (response.token) {
          this.session.token = response.token;
          this.authorizationService.autorization = this.session;
          this.authorizationService.saveSession();
          this.router.navigateByUrl('home/techs');
        }

      })
      .catch((error) => {
        console.error('error', error);
      })
      .finally(() => {
      });
  }

}
